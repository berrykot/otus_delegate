﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace otus_delegate.Tests
{
	[TestClass()]
	public class EnumerableExtTests
	{
		[TestMethod()]
		public void GetMaxTest_Car()
		{
			var carmax = new Car("sdhvfbsdjivfisdjnvcisjndvcijsndvcijsndvcijsndik");

			var carCollection = new List<Car>();
			carCollection.Add(new Car("Лада"));
			carCollection.Add(new Car("Мерседес"));
			carCollection.Add(new Car("Фольцваген"));
			carCollection.Add(carmax);
			carCollection.Add(new Car("Шеврале"));
			carCollection.Add(new Car("Митсубиси"));

			var carMaxFindByTestMethod = carCollection.GetMax(Car.GetCompareParametr);

			Assert.ReferenceEquals(carmax, carMaxFindByTestMethod);
		}


	}
}