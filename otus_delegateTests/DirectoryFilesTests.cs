﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using otus_delegate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace otus_delegate.Tests
{
	[TestClass()]
	public class DirectoryFilesTests
	{
		private const string Path = "SomeDir";
		private const string Path2 = "SomeDir2";

		[TestMethod()]
		public void ScanFilesTest()
		{
			var dir = PrepareFolder(Path);
			var filesCountRealy = PrepareFiles(dir);
			var dF = new DirectoryFiles(dir);

			var filesCounted = 0;
			dF.OnFileScaning += (s, e) => filesCounted++;

			dF.ScanFiles();

			Assert.AreEqual(filesCountRealy, filesCounted);
		}

		[TestMethod()]
		public void CancelScanTest()
		{
			var dir = PrepareFolder(Path2);
			var filesCountRealy = PrepareFiles(dir);
			var dF = new DirectoryFiles(dir);

			var fileStopScanNumber = 3;

			var filesCounted = 0;
			dF.OnFileScaning += (s, e) => {
				if (++filesCounted == fileStopScanNumber)
					((DirectoryFiles)s).CancelScan();
			};

			dF.ScanFiles();

			Assert.AreEqual(fileStopScanNumber, filesCounted);
		}

		private static DirectoryInfo PrepareFolder(string path)
		{
			if (Directory.Exists(path))
				Directory.Delete(path, true);
			var dir = Directory.CreateDirectory(path);
			return dir;
		}

		private static int PrepareFiles(DirectoryInfo dir)
		{
			var rnd = new Random(Guid.NewGuid().GetHashCode());
			int filesCount = rnd.Next(5, 9);
			for (var i = 0; i < filesCount; i++)
				File.Create(dir + $"\\test{i}.file");
			return filesCount;
		}

	}
}