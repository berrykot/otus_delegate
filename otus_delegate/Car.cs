﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus_delegate
{
	public class Car
	{
		public Car(string name)
		{
			Name = name;
		}
		public string Name { get; set; }

		public static float GetCompareParametr(Car car)
		{
			return car?.Name?.Length ?? 0;
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
