﻿using System;
using System.Collections.Generic;

namespace otus_delegate
{
	public static class EnumerableExt
	{
		public static T GetMax<T>(this IEnumerable<T> collection, Func<T, float> getParametr)
			where T : class
		{
			if (collection == null) 
				throw new ArgumentNullException(nameof(collection));
			if (getParametr == null) 
				throw new ArgumentNullException(nameof(getParametr));
			T maxValue = null;
			foreach (var item in collection)
			{
				maxValue = getParametr(maxValue) > getParametr(item) ?
					maxValue :
					item;
			}
			return maxValue;
		}
	}
}
