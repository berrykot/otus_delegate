﻿using System;
using System.Collections.Generic;
using System.IO;

namespace otus_delegate
{
	class Program
	{
		static void Main(string[] args)
		{
			//1. Посмотреть результат можно в тестовом проекте в классе EnumerableExtTests
			//2. Посмотреть результат можно в тестовом проекте в классе DirectoryFilesTests метод ScanFilesTest
			//3. Событие находится в классе DirectoryFiles
			//4. Посмотреть результат можно в тестовом проекте в классе DirectoryFilesTests метод CancelScanTest

			//5. Вывести в консоль сообщения, возникающие при срабатывании событий и результат поиска максимального элемента
			var maxCar = new List<Car>
			{
				new Car("Лада"),
				new Car("Мерседес"),
				new Car("Фольцваген"),
				new Car("Шеврале"),
				new Car("Митсубиси")
			}
			.GetMax(Car.GetCompareParametr);
			Console.WriteLine($"Автомобиль с самым длинным названием: {maxCar}");

			var df = new DirectoryFiles(Directory.GetCurrentDirectory());
			df.OnFileScaning += (s, a) => Console.WriteLine($"Собтие сработало. Найден файл: {a.FileName}");
			df.ScanFiles();
		}
	}
}
