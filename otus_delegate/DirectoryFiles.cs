﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace otus_delegate
{
	public class DirectoryFiles
	{
		private volatile bool _isCanceled;
		private readonly DirectoryInfo _dirInfo;
		public event EventHandler<FileInfoEventArgs> OnFileScaning;
		
		public DirectoryFiles(DirectoryInfo dirInfo)
		{
			_dirInfo = dirInfo ?? throw new ArgumentNullException(nameof(dirInfo));
		}

		public DirectoryFiles(string path)
			:this(new DirectoryInfo(path))
		{

		}

		public void ScanFiles()
		{
			_isCanceled = false;
			foreach (var file in GetFiles())
			{
				if (_isCanceled) return;
				OnFileScaning?.Invoke(this, new FileInfoEventArgs(file));
			}
		}
		public void CancelScan()
		{
			_isCanceled = true;
		}
		private FileInfo[] GetFiles()
		{
			return _dirInfo.GetFiles();
		}
	}
}
