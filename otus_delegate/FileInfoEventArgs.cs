﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace otus_delegate
{
	public class FileInfoEventArgs: EventArgs
	{
		public FileInfoEventArgs(FileInfo fileInfo)
		{
			FileName = fileInfo.Name;
		}

		public string FileName { get; set; }
	}
}
